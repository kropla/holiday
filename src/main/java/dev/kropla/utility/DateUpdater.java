package dev.kropla.utility;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Created by kropla on 22.06.17.
 */
@Component
public class DateUpdater {
    public LocalDate bumpUpDateWithOneMonth(LocalDate date) {
        return date.plusMonths(1).withDayOfMonth(1);
    }
}
