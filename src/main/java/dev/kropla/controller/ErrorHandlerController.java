package dev.kropla.controller;

import dev.kropla.domain.ErrorResponse;
import dev.kropla.service.datacollector.HolidayApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by kropla on 23.06.17.
 */
@RestControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler(HolidayApiException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(HolidayApiException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(Integer.parseInt(ex.getErrorCode()));
        error.setMessage(ex.getErrorMessage());
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
