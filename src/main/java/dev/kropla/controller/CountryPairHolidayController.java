package dev.kropla.controller;

import dev.kropla.domain.CountryPairQuote;
import dev.kropla.service.SharedDayHolidayService;
import dev.kropla.service.datacollector.HolidayApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by kropla on 21.06.17.
 */
@RestController
@RequestMapping(path = "/commonHoliday/search")
public class CountryPairHolidayController {

    private SharedDayHolidayService searchService;

    @Autowired
    public CountryPairHolidayController(SharedDayHolidayService searchService) {
        this.searchService = searchService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> searchCommonHolidaysForCountries(
            @Valid @ModelAttribute CountryPairQuote countryPairQuote,
            @RequestParam(defaultValue = "${holidayapi.key}") String key) throws HolidayApiException {


        ResponseEntity response = new ResponseEntity(searchService.findFirstHolidayAfterDate(countryPairQuote), HttpStatus.OK);
        return response;
    }
}
