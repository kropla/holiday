package dev.kropla.service;

import dev.kropla.domain.CountryPairQuote;
import dev.kropla.domain.SharedDayHolidays;
import dev.kropla.service.datacollector.HolidayApiException;

/**
 * Created by kropla on 22.06.17.
 */
public interface SharedDayHolidayService {

    SharedDayHolidays findFirstHolidayAfterDate(CountryPairQuote countryPairQuote) throws HolidayApiException;

}
