package dev.kropla.service.matcher;

import dev.kropla.domain.Holiday;
import dev.kropla.domain.SharedDayHolidays;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by kropla on 22.06.17.
 */
@Component
public class HolidayMatcher {

    public Optional<SharedDayHolidays> findHolidaysWIthTheSameDate(List<Holiday> firstCountryHolidays, List<Holiday> secondCountryHolidays) {
        Map<LocalDate, String> secondCountryMap = transformHolidayListToMap(secondCountryHolidays);

        Optional<Holiday> matchedHoliday = findFirstMatchedHoliday(firstCountryHolidays, secondCountryMap);

        if (matchedHoliday.isPresent()) {
            return Optional.of(createCountryPairResponse(secondCountryMap, matchedHoliday));
        }
        return Optional.empty();
    }

    private SharedDayHolidays createCountryPairResponse(Map<LocalDate, String> secondCountryMap, Optional<Holiday> matchedHoliday) {
        return new SharedDayHolidays(
                matchedHoliday.get().getDate().toString(),
                matchedHoliday.get().getName(),
                secondCountryMap.get(matchedHoliday.get().getDate()));
    }

    private Optional<Holiday> findFirstMatchedHoliday(List<Holiday> firstCountryHolidays, Map<LocalDate, String> secondCountryMap) {
        return firstCountryHolidays.stream()
                .filter(e -> secondCountryMap.containsKey(e.getDate()))
                .findFirst();
    }

    private Map<LocalDate, String> transformHolidayListToMap(List<Holiday> secondCountryHolidays) {
        return secondCountryHolidays.stream()
                .collect(Collectors.toMap(Holiday::getDate, Holiday::getName));
    }
}
