package dev.kropla.service;

import dev.kropla.domain.CountryPairQuote;
import dev.kropla.domain.Holiday;
import dev.kropla.domain.SharedDayHolidays;
import dev.kropla.service.datacollector.HolidayApiException;
import dev.kropla.service.datacollector.HolidaysCollector;
import dev.kropla.service.matcher.HolidayMatcher;
import dev.kropla.utility.DateUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by kropla on 22.06.17.
 */
@Service
public class HolidayApiService implements SharedDayHolidayService {
    private static final Logger LOG = LoggerFactory.getLogger(HolidayApiService.class);

    private HolidayMatcher matcher;
    private HolidaysCollector collector;
    private DateUpdater dateUpdater;

    @Autowired
    public HolidayApiService(HolidayMatcher matcher, HolidaysCollector collector, DateUpdater dateUpdater) {
        this.matcher = matcher;
        this.collector = collector;
        this.dateUpdater = dateUpdater;
    }

    @Override
    public SharedDayHolidays findFirstHolidayAfterDate(CountryPairQuote countryPairQuote) throws HolidayApiException {

        Optional<SharedDayHolidays> countryPair = Optional.empty();

        LocalDate date = countryPairQuote.getDate();

        while (!countryPair.isPresent()) {
            countryPair = findCommonHoliday(countryPairQuote, date);
            date = dateUpdater.bumpUpDateWithOneMonth(date);
            LOG.debug("checking for date: " + date);
        }
        return countryPair.get();
    }

    private Optional<SharedDayHolidays> findCommonHoliday(CountryPairQuote countryPairQuote, LocalDate date) throws HolidayApiException {

        List<Holiday> firstCountryHolidays = collector.findMonthHolidays(date, countryPairQuote.getFirstCountry());

        List<Holiday> secondCountryHolidays = collector.findMonthHolidays(date, countryPairQuote.getSecondCountry());

        return matcher.findHolidaysWIthTheSameDate(firstCountryHolidays, secondCountryHolidays);

    }
}
