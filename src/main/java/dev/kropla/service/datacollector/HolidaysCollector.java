package dev.kropla.service.datacollector;

import dev.kropla.domain.Holiday;
import dev.kropla.domain.HolidayApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by kropla on 22.06.17.
 */
@Component
public class HolidaysCollector {
    private static final Logger LOG = LoggerFactory.getLogger(HolidaysCollector.class);

    @Value("${endpoint.url}")
    private String endpointURL;

    @Value("${holidayapi.key}")
    private String apiKey;

    private RestTemplate restTemplate;

    @Autowired
    public HolidaysCollector(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Holiday> findMonthHolidays(LocalDate date, String countryCode) throws HolidayApiException {
        LOG.info("checking for holidays with date:{} and country: {}", date, countryCode);
        URI uri = buildUri(date, countryCode);
        Optional<HolidayApiResponse> apiResponse;
        try {
            apiResponse = Optional.of(restTemplate.getForObject(uri, HolidayApiResponse.class));
        } catch (HttpStatusCodeException ex) {
            HolidayApiException apiEx = new HolidayApiException(ex.getResponseBodyAsString());
            throw apiEx;
        }
        if (apiResponse.isPresent()) {
            return apiResponse.get()
                    .getHolidays()
                    .stream()
                    .filter(e -> e.getDate().isAfter(date))
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private URI buildUri(LocalDate date, String countryCode) {
        UriComponentsBuilder builder =
                UriComponentsBuilder.fromUriString(endpointURL)
                        .queryParam("key", apiKey)
                        .queryParam("country", countryCode)
                        .queryParam("year", date.getYear())
                        .queryParam("month", date.format(DateTimeFormatter.ofPattern("MM")));

        return builder.build().toUri();
    }
}
