package dev.kropla.service.datacollector;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by kropla on 23.06.17.
 */
public class HolidayApiException extends Exception {

    private static final long serialVersionUID = 1L;
    private static final String KEY_MESSAGE = "error";
    private static final String KEY_CODE = "status";
    private String errorMessage;
    private String errorCode;

    public String getErrorMessage() {
        return errorMessage;
    }

    public HolidayApiException(String errorMessage) {
        super();
        Map<String, String> map = Arrays.stream(errorMessage.split(","))
                .map(elem -> elem.replaceAll("[\"{}]", "").split(":"))
                .collect(Collectors.toMap(e -> e[0], e -> e[1]));
        this.errorMessage = map.get(KEY_MESSAGE);
        this.errorCode = map.get(KEY_CODE);
    }

    public String getErrorCode() {
        return errorCode;
    }
}
