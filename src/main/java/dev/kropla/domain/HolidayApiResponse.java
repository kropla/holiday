package dev.kropla.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by kropla on 22.06.17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HolidayApiResponse {

    private String status;

    List<Holiday> holidays;

    public HolidayApiResponse() {
    }

    public HolidayApiResponse(String status, List<Holiday> holidays) {
        this.status = status;
        this.holidays = holidays;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Holiday> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<Holiday> holidays) {
        this.holidays = holidays;
    }

    @Override
    public String toString() {
        return "HolidayApiResponse{" +
                "status='" + status + '\'' +
                ", holidays=" + holidays +
                '}';
    }

}
