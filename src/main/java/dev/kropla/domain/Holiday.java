package dev.kropla.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;

/**
 * Created by kropla on 22.06.17.
 */

public class Holiday {

    @NotNull
    protected String name;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate observed;

    @NotNull
    private boolean isPublic;

    public Holiday() {
    }

    public Holiday(String name, LocalDate date, LocalDate observed, boolean isPublic) {
        this.name = name;
        this.date = date;
        this.observed = observed;
        this.isPublic = isPublic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setDate(String date) {
        this.date = LocalDate.parse(date, ISO_LOCAL_DATE);
    }

    public LocalDate getObserved() {
        return observed;
    }

    public void setObserved(String observed) {
        this.observed = LocalDate.parse(observed, ISO_LOCAL_DATE);
    }

    public void setObserved(LocalDate observed) {
        this.observed = observed;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    @Override
    public String toString() {
        return "Holiday{" +
                "name='" + name + '\'' +
                ", date=" + date +
                ", observed=" + observed +
                ", isPublic=" + isPublic +
                '}';
    }

}
