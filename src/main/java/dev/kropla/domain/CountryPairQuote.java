package dev.kropla.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * Created by kropla on 22.06.17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryPairQuote {

    @NotNull
    @Size(min = 2, max = 2)
    @JsonProperty(value = "firstcountry")
    private String firstCountry;

    @NotNull
    @Size(min = 2, max = 2)
    @JsonProperty(value = "secondCountry")
    private String secondCountry;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public String getFirstCountry() {
        return firstCountry;
    }

    public void setFirstCountry(String firstCountry) {
        this.firstCountry = firstCountry;
    }

    public String getSecondCountry() {
        return secondCountry;
    }

    public void setSecondCountry(String secondCountry) {
        this.secondCountry = secondCountry;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CountryPairQuote{" +
                "firstCountry='" + firstCountry + '\'' +
                ", secondCountry='" + secondCountry + '\'' +
                ", date=" + date +
                '}';
    }
}