package dev.kropla.domain;

/**
 * Created by kropla on 22.06.17.
 */
public class SharedDayHolidays {

    private String date;

    private String name1;

    private String name2;

    public SharedDayHolidays(String date, String name1, String name2) {
        this.date = date;
        this.name1 = name1;
        this.name2 = name2;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SharedDayHolidays)) return false;

        SharedDayHolidays that = (SharedDayHolidays) o;

        if (getDate() != null ? !getDate().equals(that.getDate()) : that.getDate() != null) return false;
        if (getName1() != null ? !getName1().equals(that.getName1()) : that.getName1() != null) return false;
        return getName2() != null ? getName2().equals(that.getName2()) : that.getName2() == null;
    }

    @Override
    public int hashCode() {
        int result = getDate() != null ? getDate().hashCode() : 0;
        result = 31 * result + (getName1() != null ? getName1().hashCode() : 0);
        result = 31 * result + (getName2() != null ? getName2().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SharedDayHolidays{" +
                "date='" + date + '\'' +
                ", name1='" + name1 + '\'' +
                ", name2='" + name2 + '\'' +
                '}';
    }
}
