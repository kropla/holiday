package dev.kropla.utility;

import dev.kropla.utility.DateUpdater;
import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by kropla on 22.06.17.
 */
public class DateUpdaterTest {

    @Test
    public void whenBumpingJune_shouldReturnWithFirstJuly(){

        DateUpdater dateUpdater = new DateUpdater();
        LocalDate dateToUpdate = LocalDate.of(2016, 6, 15);
        LocalDate dateExpected = LocalDate.of(2016, 7, 1);

        assertThat(dateUpdater.bumpUpDateWithOneMonth(dateToUpdate)).isEqualTo(dateExpected);

    }
}