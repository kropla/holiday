package dev.kropla.service.matcher;

import dev.kropla.domain.SharedDayHolidays;
import dev.kropla.domain.Holiday;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by kropla on 22.06.17.
 */
public class HolidayMatcherTest {

    private List<Holiday> firstCountryHolidays;
    private List<Holiday> secondCountryHolidays;

    @Before
    public void init() {
        firstCountryHolidays = new ArrayList<>();
        firstCountryHolidays.add(
                new Holiday("holiday1",LocalDate.of(2016, 3, 2),LocalDate.of(2016, 3, 2),true));

        firstCountryHolidays.add(
                new Holiday("holiday2",LocalDate.of(2016, 3, 5),LocalDate.of(2016, 3, 5),true));

        secondCountryHolidays = new ArrayList<>();
        secondCountryHolidays.add(
                new Holiday("sec holiday1",LocalDate.of(2016, 3, 3),LocalDate.of(2016, 3, 3),true));

        secondCountryHolidays.add(
                new Holiday("sec holiday2",LocalDate.of(2016, 3, 5),LocalDate.of(2016, 3, 5),true));
    }


    @Test
    public void whenSearchingInMonth_shouldReturnFirstMatch() {

        HolidayMatcher matcher = new HolidayMatcher();
        SharedDayHolidays expected =
                new SharedDayHolidays("2016-03-05", "holiday2", "sec holiday2");

        Optional<SharedDayHolidays> matcheHoliday = matcher.findHolidaysWIthTheSameDate(firstCountryHolidays, secondCountryHolidays);


        assertThat(matcheHoliday.isPresent()).isTrue();
        assertThat(matcheHoliday.get()).isEqualTo(expected);

    }

    @Test
    public void whenSearchingWithFirstEmptyList_shouldReturnEmpty() {

        HolidayMatcher matcher = new HolidayMatcher();

        Optional<SharedDayHolidays> matcheHoliday = matcher.findHolidaysWIthTheSameDate(new ArrayList<>(), secondCountryHolidays);

        assertThat(matcheHoliday.isPresent()).isFalse();

    }

    @Test
    public void whenSearchingWithSecEmptyList_shouldReturnEmpty() {

        HolidayMatcher matcher = new HolidayMatcher();

        Optional<SharedDayHolidays> matcheHoliday = matcher.findHolidaysWIthTheSameDate(secondCountryHolidays, new ArrayList<>());

        assertThat(matcheHoliday.isPresent()).isFalse();

    }

}