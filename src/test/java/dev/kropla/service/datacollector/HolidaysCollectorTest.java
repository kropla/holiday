package dev.kropla.service.datacollector;

import dev.kropla.domain.Holiday;
import dev.kropla.domain.HolidayApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;

/**
 * Created by kropla on 22.06.17.
 */

@RunWith(MockitoJUnitRunner.class)
@TestPropertySource(value = "classpath://application-test.properties")
public class HolidaysCollectorTest {

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    private HolidaysCollector collector;

    private HolidayApiResponse apiResponse;
    private List<Holiday> holidays;

    @Before
    public void init() {
        holidays = new ArrayList<>();
        holidays.add(
                new Holiday("holiday tomorrow", LocalDate.now().plusDays(1), LocalDate.now().plusDays(1), true));

        holidays.add(
                new Holiday("holiday yesterday", LocalDate.now().minusDays(1), LocalDate.now().minusDays(1), true));

        apiResponse = new HolidayApiResponse("200", holidays);
        ReflectionTestUtils.setField(collector, "endpointURL", "https://holiday.com/v1/holidays");
        ReflectionTestUtils.setField(collector, "apiKey", "f9650e73-df81-4e25-b568-7e7c9730d34d");
    }

    @Test
    public void whenSearchForTodayDate_shouldReturnOneHoliday() throws HolidayApiException {
        Mockito.when(restTemplate.getForObject(any(), any())).thenReturn(apiResponse);

        assertThat(collector.findMonthHolidays(LocalDate.now(), "PL")).hasSize(1);
        assertThat(collector.findMonthHolidays(LocalDate.now(), "PL").get(0).getName()).isEqualTo("holiday tomorrow");
    }

    @Test
    public void whenSearchForTomorrowDate_shouldReturnEmptyResult() throws HolidayApiException {
        Mockito.when(restTemplate.getForObject(any(), any())).thenReturn(apiResponse);

        assertThat(collector.findMonthHolidays(LocalDate.now().plusDays(1), "PL")).isEmpty();
    }

    @Test
    public void whenSearchForDayBeforeYesterdayDate_shouldReturnEmptyResult() throws HolidayApiException {
        Mockito.when(restTemplate.getForObject(any(), any())).thenReturn(apiResponse);

        assertThat(collector.findMonthHolidays(LocalDate.now().minusDays(2), "PL")).hasSize(2);
        assertThat(collector.findMonthHolidays(LocalDate.now().minusDays(2), "PL")).isEqualTo(holidays);
    }

}