package dev.kropla.service;

import dev.kropla.domain.CountryPairQuote;
import dev.kropla.domain.Holiday;
import dev.kropla.domain.SharedDayHolidays;
import dev.kropla.service.datacollector.HolidayApiException;
import dev.kropla.service.datacollector.HolidaysCollector;
import dev.kropla.service.matcher.HolidayMatcher;
import dev.kropla.utility.DateUpdater;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by kropla on 22.06.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class HolidayApiServiceTest {

    private static final String FIRST_COUNTRY = "PL";
    private static final String SECOND_COUNTRY = "DE";
    private static final LocalDate DATE_HOLIDAY_SAME_DAY_AS_QUERY = LocalDate.of(2016, 10, 05);
    private static final LocalDate DATE_HOLIDAY_NEXT_TWO_MONTHS = LocalDate.of(2016, 10, 22);
    private static final LocalDate DATE_NOVEMBER = LocalDate.of(2016, 11, 01);
    private static final LocalDate DATE_DECEMBER = LocalDate.of(2016, 12, 01);
    private static final LocalDate DATE_NEXT_YEAR = LocalDate.of(2017, 01, 01);
    private List<Holiday> firstCountryHolidays;
    private List<Holiday> secondCountryHolidays;

    @Mock
    private HolidaysCollector collector;

    private HolidayMatcher matcher;
    private DateUpdater updater;

    @Before
    public void init() throws HolidayApiException {
        firstCountryHolidays = SampleData.prepareFirstCountryHolidays();

        secondCountryHolidays = SampleData.prepareSecondCountryHolidays();
        updater = new DateUpdater();
        matcher = new HolidayMatcher();

        when(collector.findMonthHolidays(DATE_HOLIDAY_SAME_DAY_AS_QUERY, FIRST_COUNTRY)).thenReturn(firstCountryHolidays.subList(2, 4));
        when(collector.findMonthHolidays(DATE_HOLIDAY_SAME_DAY_AS_QUERY, SECOND_COUNTRY)).thenReturn(secondCountryHolidays.subList(2, 3));

        when(collector.findMonthHolidays(DATE_HOLIDAY_NEXT_TWO_MONTHS, FIRST_COUNTRY)).thenReturn(new ArrayList<>());
        when(collector.findMonthHolidays(DATE_HOLIDAY_NEXT_TWO_MONTHS, SECOND_COUNTRY)).thenReturn(new ArrayList<>());

        //mocking empty month 11
        when(collector.findMonthHolidays(DATE_NOVEMBER, FIRST_COUNTRY)).thenReturn(new ArrayList<>());
        when(collector.findMonthHolidays(DATE_NOVEMBER, SECOND_COUNTRY)).thenReturn(new ArrayList<>());


        when(collector.findMonthHolidays(DATE_DECEMBER, FIRST_COUNTRY)).thenReturn(firstCountryHolidays.subList(4, 5));
        when(collector.findMonthHolidays(DATE_DECEMBER, SECOND_COUNTRY)).thenReturn(secondCountryHolidays.subList(3, 5));


        when(collector.findMonthHolidays(DATE_NEXT_YEAR, FIRST_COUNTRY)).thenReturn(firstCountryHolidays.subList(5, 6));
        when(collector.findMonthHolidays(DATE_NEXT_YEAR, SECOND_COUNTRY)).thenReturn(secondCountryHolidays.subList(5, 6));
    }

    @Test(timeout = 5000)
    public void whenQuoteInMiddleOfMonth_shouldMatchSharedAfterDate() throws HolidayApiException {

        HolidayApiService apiService = new HolidayApiService(matcher, collector, updater);

        CountryPairQuote pairQuote = new CountryPairQuote();
        pairQuote.setDate(DATE_HOLIDAY_SAME_DAY_AS_QUERY);
        pairQuote.setFirstCountry(FIRST_COUNTRY);
        pairQuote.setSecondCountry(SECOND_COUNTRY);

        SharedDayHolidays expected = new SharedDayHolidays("2016-10-20", "holiday4", "sec holiday3");


        assertThat(apiService.findFirstHolidayAfterDate(pairQuote)).isEqualTo(expected);
    }

    @Test(timeout = 5000)
    public void whenQueryIsOnAnotherTwoMonths_shouldMatchSharedInNextMonhts() throws HolidayApiException {
        HolidayApiService apiService = new HolidayApiService(matcher, collector, updater);

        CountryPairQuote pairQuote = new CountryPairQuote();
        pairQuote.setDate(DATE_HOLIDAY_NEXT_TWO_MONTHS);
        pairQuote.setFirstCountry(FIRST_COUNTRY);
        pairQuote.setSecondCountry(SECOND_COUNTRY);

        SharedDayHolidays expected = new SharedDayHolidays("2016-12-04", "holiday5 after few months", "sec holiday5 next month");

        assertThat(apiService.findFirstHolidayAfterDate(pairQuote)).isEqualTo(expected);
    }

}