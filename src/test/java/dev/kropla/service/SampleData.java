package dev.kropla.service;

import dev.kropla.domain.Holiday;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kropla on 23.06.17.
 */
public class SampleData {

    private static List<Holiday> firstCountryHolidays;
    private static List<Holiday> secondCountryHolidays;

    public static List<Holiday> prepareFirstCountryHolidays() {
        firstCountryHolidays = new ArrayList<>();
        firstCountryHolidays.add(
                new Holiday("holiday1",
                        LocalDate.of(2016, 10, 2),
                        LocalDate.of(2016, 10, 2),
                        true));

        firstCountryHolidays.add(
                new Holiday("holiday2",
                        LocalDate.of(2016, 10, 5),
                        LocalDate.of(2016, 10, 5),
                        true));

        firstCountryHolidays.add(
                new Holiday("holiday3",
                        LocalDate.of(2016, 10, 16),
                        LocalDate.of(2016, 10, 16),
                        true));

        firstCountryHolidays.add(
                new Holiday("holiday4",
                        LocalDate.of(2016, 10, 20),
                        LocalDate.of(2016, 10, 20),
                        true));

        firstCountryHolidays.add(
                new Holiday("holiday5 after few months",
                        LocalDate.of(2016, 12, 4),
                        LocalDate.of(2016, 12, 4),
                        true));

        firstCountryHolidays.add(
                new Holiday("holiday6 next year",
                        LocalDate.of(2017, 1, 2),
                        LocalDate.of(2017, 1, 2),
                        true));
        return firstCountryHolidays;
    }

    public static List<Holiday> prepareSecondCountryHolidays() {
        secondCountryHolidays = new ArrayList<>();
        secondCountryHolidays.add(
                new Holiday("sec holiday1",
                        LocalDate.of(2016, 10, 3),
                        LocalDate.of(2016, 10, 3),
                        true));

        secondCountryHolidays.add(
                new Holiday("sec holiday2",
                        LocalDate.of(2016, 10, 5),
                        LocalDate.of(2016, 10, 5),
                        true));
        secondCountryHolidays.add(
                new Holiday("sec holiday3",
                        LocalDate.of(2016, 10, 20),
                        LocalDate.of(2016, 10, 20),
                        true));

        secondCountryHolidays.add(
                new Holiday("sec holiday4 next month",
                        LocalDate.of(2016, 12, 3),
                        LocalDate.of(2016, 12, 3),
                        true));

        secondCountryHolidays.add(
                new Holiday("sec holiday5 next month",
                        LocalDate.of(2016, 12, 4),
                        LocalDate.of(2016, 12, 4),
                        true));

        secondCountryHolidays.add(
                new Holiday("sec holiday6 next year",
                        LocalDate.of(2017, 1, 2),
                        LocalDate.of(2017, 1, 2),
                        true));

        return secondCountryHolidays;
    }
}
