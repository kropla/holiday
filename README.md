# Holiday information service.

Service accept two country codes and a date in a format YYYY-MM-DD (in a x-www-form-urlencoded format).
Response is a next holiday date (after the given date) in request that will happen on the same day in both countries.

The response is in a JSON format with date and names of the holidays in both countries.

Service uses holiday Api (https://holidayapi.com) to retrieve holiday information.

### Example request (from postman application):

    POST /rest/commonHoliday/search HTTP/1.1
    Host: localhost:8080
    Content-Type: application/x-www-form-urlencoded
    Cache-Control: no-cache
    Postman-Token: 782cca20-7607-ae2c-ff40-eb1ce7049b8f
    {
        "date" : "2016-03-20",
        "firstCountry" : "PL",
        "secondCountry" : "DE"
    }



### Example response:

    {
        date: “2016-03-25”,
        name1: ”Niedziela Wielkanocna”,
        name2: “Langfredag”
    }
---

### Build and run
To build and run in command line:

    mvn clean install
    mvn spring-boot:run
    
    
#### Evidences
Proper request and response:

![Proper request and response](images/proper-request-and-response.png)


Wrong country code:

![Proper request and response](images/not-exist-country-code.png)
